<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('check-mobile-phone-number','AppController@checkMobilePhoneNumber');
Route::post('check-four-digits-pin','AppController@checkFourDigitsPin');
Route::post('user-data-fill','AppController@userDataFill');
Route::post('get-products','AppController@getProducts');
Route::post('get-order-quotation','AppController@getOrderQuotation');
Route::post('get-cart', 'AppController@getCart');
Route::post('get-delivery-slots', 'AppController@getDeliverySlots');
Route::post('check-payment-method-cash-availability', 'AppController@checkPaymentMethodCashAvailability');
Route::post('confirm-order','AppController@confirmOrder');
Route::post('get-available-days','AppController@getAvailableDays');
Route::post('get-available-times', 'AppController@getAvailableTimes');
Route::post('get-offers', 'AppController@getOffers');
Route::post('get-homepage-products', 'AppController@getHomepageProducts');

Route::post('get-user-data','AppController@getUserData');
Route::post('get-orders', 'AppController@getOrders');
Route::post('get-reservations', 'AppController@getReservations');

Route::post('confirm-reservation', 'AppController@confirmReservation');

Route::post('set-user-firebase-token', 'AppController@setUserFirebaseToken');


Route::get('test-user-bill','AppController@testUserBill');

Route::get('test-notifications','AppController@testNotification');

Route::get('test-send-email', 'AppController@testSendEmail');

Route::post('skip-phone-verification', 'AppController@skipPhoneVerification');

Route::post('verify-mobile-phone-number','AppController@verifyMobilePhoneNumber');

Route::post('user-log-out', 'AppController@userLogout');

Route::post('delete-account', 'AppController@deleteAccount');
