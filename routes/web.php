<?php

use App\Http\Controllers\Admin\OrderCrudController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/admin/dashboard',function(){
    return redirect('admin/order-custom');
});

Route::get('/test-sms','AppController@testSms');
Route::get('/test-order-email', 'AppController@testOrderEmail');
Route::get('/test-reservation-email', 'AppController@testReservationEmail');

Route::get('admin/order-status-set/{orderId?}/{statusId?}', [OrderCrudController::class, 'updateOrderStatus'])->name('updateOrderStatus');

Route::get('test',[TestController::class,'index']);

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
