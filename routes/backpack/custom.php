<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('product', 'ProductCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('offer', 'OfferCrudController');
    Route::crud('closingdate', 'ClosingdateCrudController');
    Route::crud('opening', 'OpeningCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('orderrow', 'OrderrowCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('orderstatus', 'OrderstatusCrudController');
    Route::crud('setting', 'SettingCrudController');
    Route::get('order-custom', 'OrderCrudController@showOrders')->name('showOrders');
    #Route::get('order-status-set/{orderId?}/{statusId?}', 'OrderCrudController@updateOrderStatus')->name('updateOrderStatus');
    Route::get('order-print/{orderId?}', 'OrderCrudController@printOrder')->name('printOrder');
    Route::crud('reservation', 'ReservationCrudController');
    Route::crud('notification', 'NotificationCrudController');


    Route::get('set-reservation-status/{reservationId}/{statusId}', 'ReservationCrudController@setReservationStatus')->name('setReservationStatus');
}); // this should be the absolute last line of this file
