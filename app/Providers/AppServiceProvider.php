<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Reservation;
use App\Observers\NotificationObserver;
use App\Observers\OrderObserver;
use App\Observers\ReservationObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Notification::observe(NotificationObserver::class);
        Reservation::observe(ReservationObserver::class);
        Order::observe(OrderObserver::class);
    }
}
