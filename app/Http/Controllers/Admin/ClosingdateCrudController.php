<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ClosingdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class ClosingdateCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class ClosingdateCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;


  public function setup()
  {
    $this->crud->setModel('App\Models\Closingdate');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/closingdate');
    $this->crud->setEntityNameStrings('data chiusa', 'date chiuse');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    $this->crud->setFromDb();
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(ClosingdateRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([
      'name' => 'day',
      'type' => 'date_picker',
      'label' => 'Giorno',
      // optional:
      'date_picker_options' => [
        'todayBtn' => 'linked',
        'format' => 'dd-mm-yyyy',
        'language' => 'it'
      ],
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
