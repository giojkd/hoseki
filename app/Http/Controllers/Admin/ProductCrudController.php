<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;


    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('prodotto', 'prodotti');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'enabled',
            'type' => 'boolean',
            'label' => 'Abilitato',

        ]);


        $this->crud->addColumn([
            'name' => 'cover', // The db column name
            'label' => "Immagine", // Table column heading
            'type' => 'image',
            'height' => '75px',
            'width' => '75px',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Nome',
        ]);

        $this->crud->addColumn([  // Select2
            'label' => "Categoria",
            'type' => 'select',
            'name' => 'category_id', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Category", // foreign key model

        ]);


        $this->crud->addColumn([
            'name' => 'price',
            'type' => 'text',
            'label' => 'Prezzo',
            'prefix' => '€',
        ]);

        $this->crud->addColumn([
            'name' => 'price_compare_at',
            'type' => 'text',
            'label' => 'Prezzo da comparare',
            'prefix' => '€',
        ]);

        $this->crud->addColumn([
            'name' => 'in_homepage',
            'type' => 'boolean',
            'label' => 'Homepage',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'in_homepage',
            'type' => 'checkbox',
            'label' => 'Homepage',
        ]);

        $this->crud->addField([
            'name' => 'enabled',
            'type' => 'checkbox',
            'label' => 'Abilitato',
            'default' => 1
        ]);

        $this->crud->addField([  // Select2
            'label' => "Categoria",
            'type' => 'select2',
            'name' => 'category_id', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Category", // foreign key model
            'default' => 1
        ]);


        // image
        $this->crud->addField([
            'label' => "Immagine",
            'name' => "cover",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);
        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Nome',
        ]);

        $this->crud->addField([
            'name' => 'description',
            'type' => 'textarea',
            'label' => 'Descrizione',
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'number',
            'label' => 'Prezzo',
            'prefix' => '€',
            'attributes' => ['step' => '.01']
        ]);

        $this->crud->addField([
            'name' => 'price_compare_at',
            'type' => 'number',
            'label' => 'Prezzo da comparare',
            'prefix' => '€',
            'attributes' => ['step' => '.01']
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
