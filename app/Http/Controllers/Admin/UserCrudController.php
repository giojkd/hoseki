<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class UserCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class UserCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\User');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
    $this->crud->setEntityNameStrings('cliente', 'clienti');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

    $this->crud->addClause('where','phone','!=',null);

    $this->crud->addColumn([
      'name' => 'name',
      'type' => 'text',
      'label' => 'Nome',
    ]);

    $this->crud->addColumn([
      'name' => 'surname',
      'type' => 'text',
      'label' => 'Cognome',
    ]);


    $this->crud->addColumn([
      'name' => 'phone',
      'type' => 'text',
      'label' => 'Telefono',
    ]);

    $this->crud->addColumn([
      'name' => 'email',
      'type' => 'text',
      'label' => 'Email',
    ]);

    $this->crud->addColumn([   // Address
      'name' => 'address',
      'label' => 'Indirizzo',
      'type' => 'address',
      'store_as_json' => true
    ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(UserRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();


    $this->crud->addField([
      'name' => 'name',
      'type' => 'text',
      'label' => 'Nome',
    ]);

    $this->crud->addField([
      'name' => 'surname',
      'type' => 'text',
      'label' => 'Cognome',
    ]);


    $this->crud->addField([
      'name' => 'phone',
      'type' => 'text',
      'label' => 'Telefono',
    ]);

    $this->crud->addField([
      'name' => 'email',
      'type' => 'text',
      'label' => 'Email',
    ]);

    $this->crud->addField([   // Address
      'name' => 'address',
      'label' => 'Indirizzo',
      'type' => 'address',
      'store_as_json' => true
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
