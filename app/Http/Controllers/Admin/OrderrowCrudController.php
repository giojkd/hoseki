<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderrowRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class OrderrowCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class OrderrowCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Orderrow');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/orderrow');
    $this->crud->setEntityNameStrings('riga di un ordine', 'Righe degli ordini');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();


    $this->crud->addColumn([
      'label' => "Ordine",
      'type' => 'select',
      'name' => 'order_id',
      'entity' => 'order',
      'attribute' => 'id',
      'model' => "App\Models\Order",
    ]);


    $this->crud->addColumn([
      'label' => "Prodotto",
      'type' => 'select',
      'name' => 'product_id',
      'entity' => 'product',
      'attribute' => 'name',
      'model' => "App\Models\Product",
      'default' => 1
    ]);

    $quantityOptions = [];
    for($i = 1; $i<100; $i++)
    $quantityOptions[$i] = $i;

    $this->crud->addColumn([
      'name' => 'quantity',
      'label' => 'Quantità',
      'type' => 'select_from_array',
      'options' => $quantityOptions,
      'allows_null' => false,
      'default' => '1',
    ]);


        $this->crud->addColumn([
          'name' => 'sub_total',
          'type' => 'number',
          'label' => 'Totale parziale',
          'prefix' => '€'
        ]);

  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(OrderrowRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();


    $this->crud->addField([
      'label' => "Ordine",
      'type' => 'select2',
      'name' => 'order_id',
      'entity' => 'order',
      'attribute' => 'id',
      'model' => "App\Models\Order",
    ]);


    $this->crud->addField([
      'label' => "Prodotto",
      'type' => 'select2',
      'name' => 'product_id',
      'entity' => 'product',
      'attribute' => 'name',
      'model' => "App\Models\Product",
      'default' => 1
    ]);

    $quantityOptions = [];
    for($i = 1; $i<100; $i++)
    $quantityOptions[$i] = $i;

    $this->crud->addField([
      'name' => 'quantity',
      'label' => 'Quantità',
      'type' => 'select_from_array',
      'options' => $quantityOptions,
      'allows_null' => false,
      'default' => '1',
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
