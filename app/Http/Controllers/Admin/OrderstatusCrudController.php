<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderstatusRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class OrderstatusCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class OrderstatusCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
  #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Orderstatus');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/orderstatus');
    $this->crud->setEntityNameStrings('uno stato degli ordini', 'gli stati degli ordini');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

        $this->crud->addColumn([
            'type' => 'text',
            'name' => 'name',
            'label' => 'Stato'
        ]);

        $this->crud->addColumn([
            'type' => 'boolean',
            'name' => 'show_in_orders_summary',
            'label' => 'Mostra in riepilogo degli ordini'
        ]);

        $this->crud->addColumn([
            'type' => 'boolean',
            'name' => 'hide_orders_in_orders_summary',
            'label' => 'Nascondi gli ordini in riepilogo degli ordini'
        ]);

  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(OrderstatusRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([
      'name' => 'name',
      'type' => 'text',
      'label' => 'Nome',
    ]);
        $this->crud->addField([
            'name' => 'show_in_orders_summary',
            'type' => 'checkbox',
            'label' => 'Mostra in riepilogo degli ordini',
        ]);

        $this->crud->addField([
            'name' => 'hide_orders_in_orders_summary',
            'type' => 'checkbox',
            'label' => 'Nascondi gli ordini in riepilogo degli ordini',
        ]);
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }
}
