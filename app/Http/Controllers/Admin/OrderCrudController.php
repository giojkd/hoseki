<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Order;
use App\Models\Orderstatus;
use App\Models\User;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging;
use Ladumor\OneSignal\OneSignal;
/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order');
        $this->crud->setEntityNameStrings('ordine', 'ordini');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn([
            'label' => "Cliente",
            'type' => 'select',
            'name' => 'user_id',
            'entity' => 'user',
            'attribute' => 'name',
            'model' => "App\Models\User",
        ]);

        $this->crud->addColumn([
            'name' => 'grand_total',
            'type' => 'number',
            'label' => 'Totale',
            'prefix' => '€'
        ]);

        $this->crud->addColumn([
            'label' => "Stato",
            'type' => 'select',
            'name' => 'orderstatus_id',
            'entity' => 'status',
            'attribute' => 'name',
            'model' => "App\Models\Orderstatus",
        ]);

        $this->crud->addColumn([   // DateTime
            'name' => 'delivery_time',
            'label' => 'Consegna',
            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'it'
            ],
            'allows_null' => true,
        ]);

        $this->crud->addColumn([   // Address
            'name' => 'delivery_address',
            'label' => 'Indirizzo',
            'type' => 'address_algolia',
            'store_as_json' => true
        ]);

        $this->crud->addColumn([
            'name' => 'is_paid',
            'type' => 'boolean',
            'label' => 'Pagato',
        ]);

        $this->crud->addColumn([
            'name' => 'deliverytype',
            'label' => 'Consegna',
            'type' => 'select_from_array',
            'options' => [
                'delivery' => 'Consegna a domicilio',
                'pickup' => 'Ritiro in ristorante',
            ],
            'allows_null' => false,
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(OrderRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'label' => "Cliente",
            'type' => 'select2',
            'name' => 'user_id',
            'entity' => 'user',
            'attribute' => 'name',
            'model' => "App\Models\User",
            'default' => 1
        ]);

        $this->crud->addField([
            'name' => 'grand_total',
            'type' => 'number',
            'label' => 'Totale',
            'prefix' => '€',
            'attributes' => ['step' => '.01']
        ]);

        $this->crud->addField([
            'label' => "Stato",
            'type' => 'select2',
            'name' => 'orderstatus_id',
            'entity' => 'status',
            'attribute' => 'name',
            'model' => "App\Models\Orderstatus",
            'default' => 1
        ]);

        $this->crud->addField([   // DateTime
            'name' => 'delivery_time',
            'label' => 'Consegna',
            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'it'
            ],
            'allows_null' => true,
        ]);

        $this->crud->addField([   // Address
            'name' => 'delivery_address',
            'label' => 'Indirizzo',
            'type' => 'address_algolia',
            'store_as_json' => true
        ]);

        $this->crud->addField([
            'name' => 'is_paid',
            'type' => 'checkbox',
            'label' => 'Pagato',
        ]);

        $this->crud->addField([
            'name' => 'deliverytype',
            'label' => 'Consegna',
            'type' => 'select_from_array',
            'options' => [
                'delivery' => 'Consegna a domicilio',
                'pickup' => 'Ritiro in ristorante',
            ],
            'allows_null' => false,
            'default' => '1',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function showOrders()
    {
        $data = [];
        $data['orders'] =
        Order::with(['rows', 'rows.product'])
        ->whereIn('orderstatus_id', function($query){
            $query->select('id')->from('orderstatuses')->where('hide_orders_in_orders_summary',0);
        })
        ->orderBy('delivery_time', 'ASC')
        ->get()
        ->groupBy('orderstatus_id');

        $data['statuses'] = Orderstatus::where('show_in_orders_summary', 1)->get();
        return view('customOrder', $data);
    }

    public function updateOrderStatus(Request $request, $orderId, $statusId)
    {
        $order = Order::findOrFail($orderId);
        $order->orderstatus_id = $statusId;
        $order->save();

        $orderStatus = Orderstatus::find($statusId);

        $messaging  = app('firebase.messaging');
        $user = $order->user;

        if(!is_null($user->firebase_token) && $user->firebase_token != ''){
            $deviceToken = $user->firebase_token;
            /*$notification = ['title' => 'Aggiornamento ordine', 'body' => 'Il tuo ordine è ' . $orderStatus->name,'sound' => 'default'];
            $message = CloudMessage
            ::withTarget('token', $deviceToken)
            ->withNotification($notification)
            ->withFcmOptions(['sound' => 'default']);*/
            /*$message = CloudMessage::fromArray([
                'token' => $deviceToken,
                'notification' => $notification,
                'apns' => [
                    'payload' => [
                        'aps' => [
                            'sound' => 'default'
                        ]
                    ]
                        ],
                'android' =>[
                    'notification' => [
                        'sound' => 'default'
                    ]
                ]
            ]);
            #return $message;
            $messaging->send($message);*/
            $fields['include_player_ids'] = [$deviceToken];
            $message = 'Il tuo ordine è ' . $orderStatus->name;
            $sendStatus = OneSignal::sendPush($fields, $message);
            #dd($sendStatus);
        }

        if($request->ajax()){
            return ['status' => 1];
        }else{
            return 'Status dell\'ordine aggiornato';
        }

    }

    public function printOrder($id)
    {
        $order = Order::find($id);
        return view('mails.new_order_print', ['order' => $order, 'showButtons' => false]);
    }
}
