<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OpeningRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class OpeningCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class OpeningCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Opening');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/opening');
    $this->crud->setEntityNameStrings('apertura', 'aperture');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

    $this->crud->addColumn([
      'name' => 'week_day',
      'label' => 'Giorno della settimana',
      'type' => 'select_from_array',
      'options' => [
        '1' => 'Lunedì',
        '2' => 'Martedì',
        '3' => 'Mercoledì',
        '4' => 'Giovedì',
        '5' => 'Venerdì',
        '6' => 'Sabato',
        '7' => 'Domenica',
      ],
      'allows_null' => false,
      'default' => '1',
    ]);


    $this->crud->addColumn([
      'name' => 'opens_at',
      'label' => 'Apri alle',
            'type' => 'datetime',

            'format' => 'HH:mm',

    ]);

    $this->crud->addColumn([
      'name' => 'closes_at',
      'label' => 'Chiudi alle',
            'type' => 'datetime',

            'format' => 'HH:mm',
    ]);

  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(OpeningRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();
    $this->crud->addField([
      'name' => 'week_day',
      'label' => 'Giorno della settimana',
      'type' => 'select_from_array',
      'options' => [
        '1' => 'Lunedì',
        '2' => 'Martedì',
        '3' => 'Mercoledì',
        '4' => 'Giovedì',
        '5' => 'Venerdì',
        '6' => 'Sabato',
        '7' => 'Domenica',
      ],
      'allows_null' => false,
      'default' => '1',
    ]);


    $this->crud->addField([
      'name' => 'opens_at',
      'label' => 'Apri alle',
      'type' => 'time'
    ]);

    $this->crud->addField([
      'name' => 'closes_at',
      'label' => 'Chiudi alle',
      'type' => 'time'
    ]);

  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();
  }
}
