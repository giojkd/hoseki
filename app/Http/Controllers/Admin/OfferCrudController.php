<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OfferRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
* Class OfferCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class OfferCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

  public function setup()
  {
    $this->crud->setModel('App\Models\Offer');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/offer');
    $this->crud->setEntityNameStrings('offerta', 'offerte');
  }

  protected function setupListOperation()
  {
    // TODO: remove setFromDb() and manually define Columns, maybe Filters
    #$this->crud->setFromDb();

    $this->crud->addColumn([
      'name' => 'enabled',
      'type' => 'boolean',
      'label' => 'Abilitato',

    ]);


    $this->crud->addColumn([
      'name' => 'cover', // The db column name
      'label' => "Immagine", // Table column heading
      'type' => 'image',
      'height' => '75px',
      'width' => '75px',
    ]);

    $this->crud->addColumn([
      'name' => 'name',
      'type' => 'text',
      'label' => 'Nome',
    ]);

  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(OfferRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([
      'name' => 'enabled',
      'type' => 'checkbox',
      'label' => 'Abilitato',
      'default' => 1
    ]);

    // image
    $this->crud->addField([
      'label' => "Immagine",
      'name' => "cover",
      'type' => 'image',
      'upload' => true,
      'crop' => true, // set to true to allow cropping, false to disable
      //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
      // 'disk' => 's3_bucket', // in case you need to show images from a different disk
      // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
    ]);
    $this->crud->addField([
      'name' => 'name',
      'type' => 'text',
      'label' => 'Nome',
    ]);


    $this->crud->addField([
      'name' => 'description',
      'type' => 'textarea',
      'label' => 'Descrizione',
    ]);
  }

  protected function setupUpdateOperation()
  {
    $this->setupCreateOperation();




  }
}
