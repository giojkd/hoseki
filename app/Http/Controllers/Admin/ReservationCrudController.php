<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReservationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Reservation;

/**
 * Class ReservationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReservationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Reservation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/reservation');
        $this->crud->setEntityNameStrings('prenotazione', 'prenotazioni');

        $this->crud->addButtonFromModelFunction('line', 'accept_reservation', 'acceptReservationButton', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'reject_reservation', 'rejectReservationButton', 'end');
    }

    public function setReservationStatus($reservationId, $statusId){
        $reservation = Reservation::findOrFail($reservationId);
        $reservation->reservation_status_id = $statusId;
        $reservation->save();
        return(back());
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'id',
            'type' => 'text',
            'label' => 'Num',
        ]);

        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'datetime',

            'format' => 'DD/MM/YYYY HH:mm',
            'language' => 'it',

            'label' => 'Ricevuta il',
        ]);

        $this->crud->addColumn([
            'label' => "Cliente",
            'type' => 'model_function',
            'name' => 'user_id',
            'function_name' => 'printUser'
        ]);


        $this->crud->addColumn([
            'name' => 'participants',
            'type' => 'text',
            'label' => 'Partecipanti',
        ]);

        $this->crud->addColumn([
            'name' => 'reservation_time',
            'type' => 'datetime',

                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'it',

            'label' => 'Quando',
        ]);


        $this->crud->addColumn([
            'name' => 'reservation_status_id',
            'type' => 'select_from_array',
            'options' => [
                1 => 'Ricevuta',
                2 => 'Accettata',
                3 => 'Rifiutata',
            ],
            'label' => 'Stato',
        ]);




    }

    protected function setupCreateOperation()
    {
        #$this->crud->setValidation(ReservationRequest::class);

        $this->crud->addField([
            'label' => "Cliente",
            'type' => 'select2',
            'name' => 'user_id',
            'entity' => 'user',
            'attribute' => 'name',
            'model' => "App\Models\User",
            'default' => 1
        ]);

        $this->crud->addField([
            'name' => 'participants',
            'type' => 'text',
            'label' => 'Partecipanti',
        ]);

        $this->crud->addField([
            'name' => 'reservation_time',
            'type' => 'datetime_picker',
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'it'
            ],
            'label' => 'Quando',
        ]);


        $this->crud->addField([
            'name' => 'reservation_status_id',
            'type' => 'select_from_array',
            'options' => [
                1 => 'Ricevuta',
                2 => 'Accettata',
                3 => 'Rifiutata',
            ],
            'label' => 'Stato',
        ]);



        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
