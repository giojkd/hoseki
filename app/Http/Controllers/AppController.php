<?php

namespace App\Http\Controllers;

use App\Mail\NewOrder;
use App\Mail\NewReservation;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Closingdate;
use App\Models\Opening;
use App\Models\Order;
use App\Models\Orderrow;
use App\Models\Reservation;
use App\Models\Offer;
use App\Models\Setting;
use Exception;
use jobayerccj\Skebby\Skebby;
use Validator;
use Illuminate\Support\Facades\Mail;

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging;







class AppController extends Controller
{


    public function testReservationEmail()
    {
        Mail::to(env('EMAIL_NOTIFICATIONS_RECIPIENT'))->send(new NewReservation(Reservation::find(1)));
    }

    public function testOrderEmail()
    {

        Mail::to(env('EMAIL_NOTIFICATIONS_RECIPIENT'))->send(new NewOrder(Order::find(8)));
    }


    public function userLogout(Request $request)
    {
        $user = User::find($request->UserId);
        $user->firebase_token = null;
        $user->save();
    }

    public function deleteAccount(Request $request)
    {
        $user = User::find($request->UserId);
        $user->orders()->delete();
        $user->reservations()->delete();
        $user->delete();
    }



    public function verifyMobilePhoneNumber(Request $request)
    {

        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'MobilePhoneNumber' => 'required|numeric',
                'UserId' => 'required|numeric',
            ],
            $messages
        );

        if ($validator->fails()) {
            return ['status' => 0, 'message' => 'Il numero inserito non è valido.'];
        }

        $userByPhone = User::where('phone', $request->MobilePhoneNumber)->first();

        if ($userByPhone) {
            return ['status' => 0, 'message' => 'Il numero appartiene già ad un altro utente.'];
        }

        $user = User::find($request->UserId);

        $user->phone_temporary = $request->MobilePhoneNumber;
        $user->save();

        $fourDigitsCode = $user->generatePin();

        $skebby = new Skebby;
        $skebby->set_username(env('SKEBBY_USERNAME'));
        $skebby->set_password(env('SKEBBY_PASSWORD'));
        $skebby->set_method(env('SKEBBY_METHOD'));
        $skebby->set_text('Il codice è ' . $fourDigitsCode);
        $skebby->set_sender(env('SKEBBY_SENDER'));

        $smsRecipients = ['+39' . $request->MobilePhoneNumber];

        $skebby->set_recipients($smsRecipients);

        $sendingStatus = $skebby->send_sms();

        $return = ['status' => 1, 'message' => '', 'sending_status' => $sendingStatus, 'user_id' => $user->id, 'code' => $fourDigitsCode];

        return $return;
    }

    public function skipPhoneVerification(Request $request)
    {
        $user = User::create([]);
        return ['status' => 1, 'id' => $user->id];
    }

    public function testSendEmail()
    {
        $order = Order::find(1);
        Mail::to('giojkd@gmail.com')->send(new NewOrder($order));
    }

    public function testNotification()
    {
        $messaging  = app('firebase.messaging');
        $user = User::find(2);
        $deviceToken = $user->firebase_token;
        $data = [
            'first_key' => 'First Value',
            'second_key' => 'Second Value',
        ];
        $notification = ['title' => 'My title', 'body' => 'My Body'];
        $message = CloudMessage::withTarget('token', $deviceToken)
            ->withNotification($notification) // optional
            ->withData($data) // optional
        ;
        $messaging->send($message);
    }

    public function setUserFirebaseToken(Request $request)
    {
        $UserId = $request->UserId;
        $User = User::find($UserId);
        $User->firebase_token = $request->FirebaseToken;
        $User->save();
    }

    public function getUserData(Request $request)
    {
        $UserId = $request->UserId;
        $User = User::find($UserId);
        return $User;
    }

    public function getOrders(Request $request)
    {
        $UserId = $request->UserId;
        $User = User::find($UserId);
        $orders = $User->orders->map(function ($item) {
            $item['created_at_custom'] = $item->created_at->format('d/m/Y H:i');
            return $item;
        });
        return ['orders' => $orders];
    }

    public function getReservations(Request $request)
    {
        $UserId = $request->UserId;
        $User = User::find($UserId);
        $reservations = $User->reservations->map(function ($item) {
            $status = [1 => 'In attesa', 2 => 'Accettata', 3 => 'Rifiutata'];
            $item['reservation_time_custom'] = $item->reservation_time->format('d/m/Y H:i');
            $item['status'] = $status[$item->reservation_status_id];
            return $item;
        });
        return ['reservations' => $reservations];
    }

    public function confirmReservation(Request $request)
    {

        #        return $request->all();

        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'UserId'  => 'required|numeric',
                'participants' => 'required|numeric',
                'dayValue' => 'required',
                'timeValue' => 'required',
            ],
            $messages
        );

        if ($validator->fails()) {
            return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
        }


        $reservation = Reservation::create([
            'user_id' => $request->UserId,
            'participants' => $request->participants,
            'reservation_time' => $request->timeValue,
        ]);


        return ['status' => 1];
    }

    public function confirmOrder(Request $request)
    {


        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $user = User::find($request->UserId);



        if (\is_null($user->name)) {
            $validatorRules = [];
            $validatorRules['UserName'] = 'required';
            $validatorRules['UserSurname'] = 'required';
            $validatorRules['UserEmail'] = 'required|email';

            $validator = Validator::make(
                $request->all(),
                $validatorRules,
                $messages
            );
            if ($validator->fails()) {
                return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
            }
        }

        if ($request->deliveryMethod == 'delivery' && $request->UserAddress == '') {
            return ['status' => 0, 'message' => 'L\'indirizzo di consegna è obbligatorio'];
        }

        if (!$request->deliverAsap && is_null($request->deliveryTimeValue)) {
            return ['status' => 0, 'message' => 'L\'orario di consegna/ritiro è obbligatorio'];
        }

        $order = new Order();

        if (!$request->deliverAsap) {
            if (Closingdate::isDateClosed($request->deliveryTimeValue)) {
                return ['status' => 0, 'message' => 'Il ristorante è chiuso nella data di consegna selezionata.'];
            }
            #if user has scheduled the delivery
            $order->delivery_time = $request->deliveryTimeValue;
        } else {
            #if user wants the order delivered asap
            if (Closingdate::isTodayClosed()) {
                return ['status' => 0, 'message' => 'Il ristorante oggi è chiuso.'];
            }
        }


        if (\is_null($user->name)) {

            $userCheckEmail = User::where('email', $request->UserEmail)->first();
            if (!is_null($userCheckEmail)) {
                return ['status' => 0, 'message' => 'L\'indirizzo email inserito appartiene già ad un altro utente'];
            }
            $user->fill(
                [
                    'name' => $request->UserName,
                    'surname' => $request->UserSurname,
                    'email' => $request->UserEmail
                ]
            );
            $user->save();
        }



        if ($request->PaymentMethod == 'newcard') {

            $validatorRules = [
                'CreditCardCvv' => 'required|numeric',
                'CreditCardExpMm' => 'required|numeric',
                'CreditCardExpYy' => 'required|numeric',
                'CreditCardHolderName' => 'required',
                'CreditCardNumber' => 'required|numeric'
            ];

            $validator = Validator::make(
                $request->all(),
                $validatorRules,
                $messages
            );
            if ($validator->fails()) {
                return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
            }
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            try {
                $token = \Stripe\Token::create(
                    array(
                        "card" => array(
                            "name" => $request->CreditCardHolderName,
                            "number" => $request->CreditCardNumber,
                            "exp_month" => $request->CreditCardExpMm,
                            "exp_year" => $request->CreditCardExpYy,
                            "cvc" => $request->CreditCardCvv
                        )
                    )
                );
            } catch (\Stripe\Exception\CardException $e) {
                $error = $e->getMessage();
                return ['status' => 0, 'message' => $error];
            } catch (\Stripe\Error\Card $e) {
                $error = $e->getMessage();
                return $error;
            } catch (\Stripe\Error\InvalidRequest $e) {
                $error = $e->getJsonBody();
                return $error;
            }


            try{
                $customer = \Stripe\Customer::create([
                    'source' => $token->id,
                    'email' => $user->email
                ]);
            }catch(Exception $e){
                return $e->getMessage();
            }


            //if ($request->saveNewCard) {
            $user->fill([
                'stripe_id' => $customer->id,
                #'card_brand' => $customer->sources->data[0]->brand,
                #'card_last_four' => $customer->sources->data[0]->last4
            ]);
            $user->save();
            //}
        }



        $quotation = $user->getQuotation();


        $orderProducts = $quotation['products'];



        $order->user_id = $user->id;
        $order->deliverytype = $request->deliveryMethod;
        if ($request->deliveryMethod == 'delivery') {
            $order->delivery_address = ['value' => $request->UserAddress];
        }

        $toBill =  $quotation['quotation'] * 100;
        if ($request->deliveryMethod == 'delivery') {
            $deliveryExpenses = Setting::find('delivery_price')->value;
            $toBill += $deliveryExpenses * 100;
        }

        if ($request->PaymentMethod == 'newcard' || $request->PaymentMethod == 'oldcard') {
            $user->invoiceFor('Ordine su Hoseki', $toBill);
            $order->is_paid = 1;
        } else {
            $order->is_paid = 0;
        }


        $order->notes = $request->OrderNotes;

        $order->grand_total = $toBill / 100;

        $order->save();

        #return $quotation['products'];

        foreach ($quotation['products'] as $product) {

            $row = new Orderrow();
            $row->fill([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'quantity' => $product->quantity,
                'sub_total' => $product->price * $product->quantity
            ]);
            $row->save();
        }

        try{
            Mail::to(env('EMAIL_NOTIFICATIONS_RECIPIENT'))->send(new NewOrder($order, true));           #sends to restaurant, includes buttons
            Mail::to($user->email)->send(new NewOrder($order, false));                                  #sends to client, NOT includes buttons
        }catch(Exception $e){

        }


        $user->cart = Null;

        $user->save();

        return ['status' => 1, 'order_id' => $order->id];
    }

    public function testUserBill()
    {
        $user = User::find(2);
        $user->invoiceFor('One Time Fee', 500);
    }

    public function getHomepageProducts(Request $request)
    {
        $products = Product::where('in_homepage', 1)->where('enabled', 1)->orderBy('name', 'ASC')->get();
        $homepageTextActive = Setting::find('homepage_text_active')->value;

        $homepageText = '';
        if ($homepageTextActive) {
            $homepageText = Setting::find('homepage_text')->value; #homepage_text
        }

        return [
            'products' => $products,
            'homepageMessage' => $homepageText
        ];
    }

    public function getOffers(Request $request)
    {
        $offers = Offer::where('enabled', 1)->orderBy('id', 'DESC')->get();
        return $offers;
    }

    public function getAvailableTimes(Request $request)
    {
        $day = date('N', strtotime($request->day));
        $openings = Opening::where('week_day', $day)->orderBy('week_day', 'ASC')->orderBy('opens_at', 'ASC')->get();
        $step = Setting::find('delivery_slots_step')->value;
        $slots = [];
        foreach ($openings as $opening) {
            $opens_at = (date('H:i', strtotime($opening->opens_at)));
            $slot_right_edge = (date('H:i', strtotime($opens_at . '+' . $step . 'minutes')));
            while ($opens_at < $opening->closes_at) {
                $slots[] = [
                    'text' => $opens_at,
                    'value' => date('Y-m-d', strtotime($request->day)) . ' ' . $opens_at
                ];
                $opens_at = (date('H:i', strtotime($opens_at . '+' . ($step) . 'minutes')));
            }
        }

        return ['name' => 'time', 'options' => $slots];
    }

    public function getAvailableDays(Request $request)
    {

        $dayNames = [
            1 => 'Lunedì',
            2 => 'Martedì',
            3 => 'Mercoledì',
            4 => 'Giovedì',
            5 => 'Venerdì',
            6 => 'Sabato',
            7 => 'Domenica',
        ];
        $monthNames = [
            1 => 'Gennaio',
            2 => 'Febbraio',
            3 => 'Marzo',
            4 => 'Aprile',
            5 => 'Maggio',
            6 => 'Giugno',
            7 => 'Luglio',
            8 => 'Agosto',
            9 => 'Settembre',
            10 => 'Ottobre',
            11 => 'Novembre',
            12 => 'Dicembre',
        ];

        $today = date('Ymd');
        $tomorrow = date('Ymd', strtotime('+1days'));
        $specialDayNames = [
            $today => 'Oggi',
            $tomorrow => 'Domani',
        ];
        $currentDay = date('Ymd');
        $lastDay = date('Ymd', strtotime('+21days'));
        $closedDates = Closingdate::whereBetween('day', [$currentDay, $lastDay])->orderBy('day', 'ASC')->get()->pluck('day')->map(function ($item) {
            return date('Ymd', strtotime($item));
        });
        $openings = Opening::orderBy('week_day', 'ASC')->orderBy('opens_at', 'ASC')->get()->groupBy('week_day');
        $slots = [];
        while ($currentDay < $lastDay) {
            if (!$closedDates->contains($currentDay)) {
                $weekDay = date('N', strtotime($currentDay));
                if (isset($openings[$weekDay])) {
                    if (isset($specialDayNames[$currentDay])) {
                        $text = $specialDayNames[$currentDay];
                    } else {
                        $unixDay = strtotime($currentDay);
                        $text = $dayNames[date('N', $unixDay)] . ' ' . date('j', $unixDay) . ' ' . $monthNames[date('n', $unixDay)];
                    }
                    $slots[] = [
                        'value' => $currentDay,
                        'text' => $text
                    ];
                }
            }
            $currentDay = date('Ymd', strtotime($currentDay . '+1days'));
        }
        return ['name' => 'day', 'options' => $slots];
    }



    public function checkPaymentMethodCashAvailability(Request $request)
    {
        $UserId = $request->UserId;
        $user = User::find($UserId);
        $quotation = $user->getQuotation();

        $method = $request->method;
        $cashOk = Setting::find('delaied_payment_for_' . $method)->value;
        $notes = Setting::find('notes_for_' . $method)->value;

        if ($method == 'delivery') {
            $deliveryExpenses = Setting::find('delivery_price')->value;
        } else {
            $deliveryExpenses = 0;
        }

        return [
            'status' => ($cashOk == 1) ? true : false,
            'deliveryExpenses' => $deliveryExpenses,
            'quotation' => number_format($quotation['quotation'] + $deliveryExpenses, 2),
            'notes' => $notes
        ];
    }

    public function getDeliverySlots(Request $request)
    {
        $step = Setting::find('delivery_slots_step')->value;

        $today = date('Ymd');
        $currentDay = date('Ymd');
        $lastDay = date('Ymd', strtotime('+2days'));
        $closedDates = Closingdate::whereBetween('day', [$currentDay, $lastDay])->orderBy('day', 'ASC')->get()->pluck('day')->map(function ($item) {
            return date('Ymd', strtotime($item));
        });
        $openings = Opening::orderBy('week_day', 'ASC')->orderBy('opens_at', 'ASC')->get()->groupBy('week_day');
        $slots = [];
        while ($currentDay < $lastDay) {
            if (!$closedDates->contains($currentDay)) {
                $weekDay = date('N', strtotime($currentDay));
                if (isset($openings[$weekDay])) {
                    foreach ($openings[$weekDay] as $opening) {
                        $opens_at = (date('H:i', strtotime($opening->opens_at)));
                        $slot_right_edge = (date('H:i', strtotime($opens_at . '+' . $step . 'minutes')));
                        while ($slot_right_edge < $opening->closes_at) {

                            $slots[] = [
                                'text' => ($currentDay == $today) ? 'Oggi ' . $opens_at . ' - ' . $slot_right_edge : 'Domani ' . $opens_at . ' - ' . $slot_right_edge,
                                'value' => date('Y-m-d', strtotime($currentDay)) . ' ' . $opens_at
                            ];
                            $opens_at = $slot_right_edge;
                            $slot_right_edge = (date('H:i', strtotime($opens_at . '+' . ($step) . 'minutes')));
                        }
                    }
                }
            }
            $currentDay = date('Ymd', strtotime($currentDay . '+1days'));
        }
        return ['name' => 'slot', 'options' => $slots];
    }

    public function getOrderQuotation(Request $request)
    {
        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'UserId' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
        }

        $method = $request->deliveryMethod;
        if ($method == 'delivery') {
            $deliveryExpenses = Setting::find('delivery_price')->value;
        } else {
            $deliveryExpenses = 0;
        }


        $user = User::findOrFail($request->UserId);
        $user->cart = $sp = collect($request->selectdProducts);
        $user->save();

        $sp = collect($request->selectdProducts);
        if ($sp->count() > 0) {

            return ['quotation' => number_format($user->getQuotation()['quotation'] + $deliveryExpenses, 2)];
        }
        return ['quotation' => 0];
    }

    public function getCart(Request $request)
    {
        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'UserId' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
        }



        $user = User::findOrFail($request->UserId);
        $return = $user->getQuotation();
        $return['userHasCard'] = (!is_null($user['stripe_id'])) ? true : false;
        $return['defaultPaymentMethod'] = (!is_null($user['stripe_id'])) ? 'oldcard' : 'newcard';
        $return['user'] = $user;

        return $return;
    }

    public function getProducts(Request $request)
    {
        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'UserId' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
        }

        $quotation = 0;

        $categories = Category::with(['products' => function ($query) {
            $query->where('enabled', 1)->orderBy('name', 'ASC');
        }])
            ->orderBy('id', 'ASC')
            ->get();

        $user = User::findOrFail($request->UserId);
        $cart = collect($user->cart);
        $quotation = $user->getQuotation()['quotation'];

        if ($cart->count() > 0) {
            $cart->each(function ($cartEl) use ($categories) {
                $categories->each(function ($cat) use ($cartEl) {
                    $cat->products->each(function ($product) use ($cartEl) {
                        if ($product->id == $cartEl['id']) {
                            $product['quantity'] = $cartEl['quantity'];
                        }
                    });
                });
            });
        }






        return [
            'categories' => $categories,
            'status' => 1,
            'quotation' => $quotation
        ];
    }

    public function userDataFill(Request $request)
    {
        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'UserId' => 'required|numeric',
                /*
                    'CreditCardCvv' => 'required|numeric',
                    'CreditCardExpMm' => 'required|numeric',
                    'CreditCardExpYy' => 'required|numeric',
                    'CreditCardHolderName' => 'required',
                    'CreditCardNumber' => 'required|numeric',
                */
                'UserAddress' => 'required',
                'UserEmail' => 'required|unique:users,email',
                'UserName' => 'required',
                'UserSurname' => 'required'
            ],
            $messages
        );

        if ($validator->fails()) {
            return ['status' => 0, 'message' => collect($validator->errors())->flatten()->join(', ')];
        }

        $user = User::find($request->UserId);

        /*
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
            $token = \Stripe\Token::create(
                array(
                    "card" => array(
                        "name" => $request->CreditCardHolderName,
                        "number" => $request->CreditCardNumber,
                        "exp_month" => $request->CreditCardExpMm,
                        "exp_year" => $request->CreditCardExpYy,
                        "cvc" => $request->CreditCardCvv
                    )
                )
            );
        } catch (\Stripe\Exception\CardException $e) {
            $error = $e->getMessage();

            return ['status' => 0, 'message' => $error];
        } catch (\Stripe\Error\Card $e) {
            $error = $e->getMessage();
            return $error;
        } catch (\Stripe\Error\InvalidRequest $e) {
            $error = $e->getJsonBody();
            return $error;
        }
        $customer = \Stripe\Customer::create([
            'source' => $token->id,
            'email' => $request->UserEmail
        ]); */
        $user->fill([
            /* 'stripe_id' => $customer->id,
            'card_brand' => $customer->sources->data[0]->brand,
            'card_last_four' => $customer->sources->data[0]->last4, */
            'name' => $request->UserName,
            'surname' => $request->UserSurname,
            'address' => $request->UserAddress,
            'email' => $request->UserEmail,
        ]);
        $user->save();
        return ['status' => 1];
    }

    public function checkFourDigitsPin(Request $request)
    {
        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'FourDigitsPin' => 'required|numeric',
                'UserId' => 'required|numeric'
            ],
            $messages
        );

        if ($validator->fails()) {
            return ['status' => 0, 'message' => 'Il codice inserito non è valido.'];
        }

        $user = User::where('id', $request->UserId)->where('four_digits_pin', $request->FourDigitsPin)->first();



        $mustFillData = false;

        if ($user) {
            if (is_null($user->name)) {
                $mustFillData = 1;
            }
            $status = 1;
            $message = '';
            if (!is_null($user->phone_temporary)) {
                $user->phone = $user->phone_temporary;
                $user->phone_temporary = null;
                $user->save();
            }
        } else {

            $status = 0;
            $message = 'Il codice inserito non è valido';
        }


        $return = ['status' => $status, 'message' => $message, 'must_fill_data' => $mustFillData];

        return $return;
    }

    public function checkMobilePhoneNumber(Request $request)
    {

        $messages = [
            'required' => 'Il campo :attribute è obbligatorio.',
        ];

        $validator = Validator::make(
            $request->all(),
            [
                'MobilePhoneNumber' => 'required|numeric'
            ],
            $messages
        );

        if ($validator->fails()) {
            return ['status' => 0, 'message' => 'Il numero inserito non è valido.'];
        }

        $user = User::where('phone', $request->MobilePhoneNumber)->first();




        if ($user) {
            $status = 1;
            $message = '';
        } else {
            $status = 2;
            $message = '';
            $user = User::create(['phone' => $request->MobilePhoneNumber]);
        }


        $fourDigitsCode = $user->generatePin();

        $skebby = new Skebby;
        $skebby->set_username(env('SKEBBY_USERNAME'));
        $skebby->set_password(env('SKEBBY_PASSWORD'));
        $skebby->set_method(env('SKEBBY_METHOD'));
        $skebby->set_text('Il codice è ' . $fourDigitsCode);
        $skebby->set_sender(env('SKEBBY_SENDER'));

        $smsRecipients = ['+39' . $request->MobilePhoneNumber];

        $skebby->set_recipients($smsRecipients);

        $sendingStatus = $skebby->send_sms();

        $return = ['status' => $status, 'message' => $message, 'sending_status' => $sendingStatus, 'user_id' => $user->id];

        return $return;
    }

    public function testSms()
    {
        $skebby = new Skebby;
        $skebby->set_username(env('SKEBBY_USERNAME'));
        $skebby->set_password(env('SKEBBY_PASSWORD'));
        $skebby->set_method(env('SKEBBY_METHOD'));
        $skebby->set_text('Prova');
        $skebby->set_sender(env('SKEBBY_SENDER'));

        $smsRecipients = ['+393296378400'];

        $skebby->set_recipients($smsRecipients);

        $sendingStatus = $skebby->send_sms();

        dd($sendingStatus);
    }
}
