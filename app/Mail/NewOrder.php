<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;

class NewOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

        public $order;
        public $showButtons;

    public function __construct(Order $order,$showButtons)
    {
        //
        $this->order = $order;
        $this->showButtons = $showButtons;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->from('noreply@hosekifirenze.com')
        ->subject('Nuovo ordine | App')
        ->view('mails.new_order');
    }
}
