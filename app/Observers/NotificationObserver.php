<?php


namespace App\Observers;

use App\Models\Notification;
use App\Models\User;

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging;
use OneSignal;

class NotificationObserver
{
    /**
     * Handle the notification "created" event.
     *
     * @param  \App\Notification  $notification
     * @return void
     */


    public function created(Notification $notification)
    {
        $users = User::all();


        $messaging  = app('firebase.messaging');

        $data = [
            'first_key' => $notification->title,
            'second_key' => $notification->content,
        ];

        #$notification = ['title' => $notification->title, 'body' => $notification->content];

        $users->each(function ($user) use ($notification, $messaging) {
            if (!is_null($user->firebase_token) && $user->firebase_token != '') {


                /*$deviceToken = $user->firebase_token;

                #                $message = CloudMessage::withTarget('token', $deviceToken)
                #                   ->withNotification($notification) // optional
                #->withData($data) // optional
                #               ;

                try {
                    $message = CloudMessage::fromArray([
                        'token' => $deviceToken,
                        'notification' => $notification,
                        'apns' => [
                            'payload' => [
                                'aps' => [
                                    'sound' => 'default'
                                ]
                            ]
                        ],
                        'android' => [
                            'notification' => [
                                'sound' => 'default'
                            ]
                        ]
                    ]);
                    $messaging->send($message);
                } catch (\Kreait\Firebase\Exception\Messaging\NotFound $e) {
                }
                */

                $fields['include_player_ids'] = [$user->firebase_token];
                $message = $notification->content;
                OneSignal::sendPush($fields, $message);



            }
        });
    }

    /**
     * Handle the notification "updated" event.
     *
     * @param  \App\Notification  $notification
     * @return void
     */
    public function updated(Notification $notification)
    {
        //
    }

    /**
     * Handle the notification "deleted" event.
     *
     * @param  \App\Notification  $notification
     * @return void
     */
    public function deleted(Notification $notification)
    {
        //
    }

    /**
     * Handle the notification "restored" event.
     *
     * @param  \App\Notification  $notification
     * @return void
     */
    public function restored(Notification $notification)
    {
        //
    }

    /**
     * Handle the notification "force deleted" event.
     *
     * @param  \App\Notification  $notification
     * @return void
     */
    public function forceDeleted(Notification $notification)
    {
        //
    }
}
