<?php

namespace App\Observers;

use App\Models\Reservation;

use App\Models\User;

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewReservation;
use OneSignal;


class ReservationObserver
{
    /**
     * Handle the reservation "created" event.
     *
     * @param  \App\Reservation  $reservation
     * @return void
     */
    public function created(Reservation $reservation)
    {
        //
        Mail::to(env('EMAIL_NOTIFICATIONS_RECIPIENT'))->send(new NewReservation($reservation));

    }

    /**
     * Handle the reservation "updated" event.
     *
     * @param  \App\Reservation  $reservation
     * @return void
     */
    public function updated(Reservation $reservation)
    {
        $statusOptions = [1 => 'In attesa di valutazione',2=>'Accettata',3=>'Rifiutata'];
        $status = $statusOptions[$reservation->reservation_status_id];
        $user = $reservation->user;
        $messaging  = app('firebase.messaging');
        $notification = ['title' => 'Aggiornamento sulla tua prenotazione', 'body' => 'La tua prenotazione è stata ' . $status];

        if (!is_null($user->firebase_token)) {

            $deviceToken = $user->firebase_token;

            /*#$message = CloudMessage::withTarget('token', $deviceToken)->withNotification($notification);
            $message = CloudMessage::fromArray([
                'token' => $deviceToken,
                'notification' => $notification,
                'apns' => [
                    'payload' => [
                        'aps' => [
                            'sound' => 'default'
                        ]
                    ]
                ],
                'android' => [
                    'notification' => [
                        'sound' => 'default'
                    ]
                ]
            ]);
            $messaging->send($message);*/

            $fields['include_player_ids'] = [$deviceToken];
            $message = 'La tua prenotazione è stata ' . $status;
            OneSignal::sendPush($fields, $message);

        }

    }

    /**
     * Handle the reservation "deleted" event.
     *
     * @param  \App\Reservation  $reservation
     * @return void
     */
    public function deleted(Reservation $reservation)
    {
        //
    }

    /**
     * Handle the reservation "restored" event.
     *
     * @param  \App\Reservation  $reservation
     * @return void
     */
    public function restored(Reservation $reservation)
    {
        //
    }

    /**
     * Handle the reservation "force deleted" event.
     *
     * @param  \App\Reservation  $reservation
     * @return void
     */
    public function forceDeleted(Reservation $reservation)
    {
        //
    }
}
