<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'reservations';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['reservation_time' => 'datetime'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function acceptReservationButton($crud = false)
    {
        return '<a class="btn btn-sm btn-link btn-success" style="color:white" href="' . route('setReservationStatus', ['reservationId' => $this->id, 'statusId' => 2]) . '" data-toggle="tooltip" title="Accetta la prenotazione">Accetta</a>';
    }

    public function rejectReservationButton($crud = false)
    {
        return '<a class="btn btn-sm btn-link btn-danger" style="color:white" href="' . route('setReservationStatus', ['reservationId' => $this->id, 'statusId' => 3]) . '" data-toggle="tooltip" title="Accetta la prenotazione">Rifiuta</a>';
    }

    public function printUser()
    {
        $return = [];
        if (!is_null($this->user)) {
            if (!is_null($this->user->name)) {
                $return[] = $this->user->name;
            }

            if (!is_null($this->user->surname)) {
                $return[] = $this->user->surname;
            }

            if (!is_null($this->user->phone)) {
                $return[] = '(' . $this->user->phone . ')';
            }
        }
        return implode(' ', $return);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
