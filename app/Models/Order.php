<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Date;
class Order extends Model
{
  use CrudTrait;

  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'orders';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  // protected $fillable = [];
  // protected $hidden = [];
  // protected $dates = [];
  protected $with = ['status'];
  protected $casts = ['delivery_time' => 'datetime','delivery_address' => 'json'];

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  public function user(){
    return $this->belongsTo('App\Models\User');
  }

  public function status(){
    return $this->belongsTo('App\Models\Orderstatus','orderstatus_id');
  }

  public function rows(){
      return $this->hasMany('App\Models\Orderrow');
  }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
/*
  public function setDeliveryTimeAttribute($value) {
    $this->attributes['delivery_time'] = \Date::parse($value);
  }
*/
}
