<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;


class User extends Model
{
    use CrudTrait;
    use Billable;


    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'users';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['address' => 'json','cart' => 'json'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function generatePin(){
      $this->four_digits_pin = rand(1000,9999);

        if($this->phone == '3333333333'){
            $this->four_digits_pin = 1234;
        }

      $this->save();
      return $this->four_digits_pin;
    }

    public function getQuotation(){
        $sp = collect($this->cart);
        $products = Product::find($sp->pluck('id'));
        $quotation = 0;
        $qts = $sp->pluck('quantity', 'id');
        foreach ($products as $key => $product) {
            $quotation += $product->price * $qts[$product->id];
            $products[$key]['quantity'] = $qts[$product->id];
        }
        return ['quotation' => $quotation,'products' => $products,'cartProducts' => $sp];
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function orders(){
        return $this->hasMany('App\Models\Order');
    }


    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
