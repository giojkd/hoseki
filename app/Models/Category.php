<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Str;

class Category extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'categories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function products(){
      return $this->hasMany('App\Models\Product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */


      public function setCoverAttribute($value)
      {
        $attribute_name = "cover";
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads";
        if ($value==null) {
          \Storage::disk($disk)->delete($this->{$attribute_name});
          $this->attributes[$attribute_name] = null;
        }
        if (Str::startsWith($value, 'data:image'))
        {
          $image = \Image::make($value)->encode('jpg', 90);
          $filename = md5($value.time()).'.jpg';
          \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
          \Storage::disk($disk)->delete($this->{$attribute_name});
          $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
          $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
        }


      }

}
