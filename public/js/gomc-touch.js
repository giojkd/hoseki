$(function () {
    $.extend($.support, {
        touch: "ontouchend" in document
    }), $.support.touch && (document.addEventListener("touchstart", iPadTouchHandler, !1), document.addEventListener("touchmove", iPadTouchHandler, !1), document.addEventListener("touchend", iPadTouchHandler, !1), document.addEventListener("touchcancel", iPadTouchHandler, !1))
});
var lastTap = null,
    tapValid = !1,
    tapTimeout = null;

function cancelTap() {
    tapValid = !1
}
var rightClickPending = !1,
    rightClickEvent = null,
    holdTimeout = null,
    cancelMouseUp = !1;

function cancelHold() {
    rightClickPending && (window.clearTimeout(holdTimeout), rightClickPending = !1, rightClickEvent = null)
}

function startHold(e) {
    rightClickPending || (rightClickPending = !0, rightClickEvent = e.changedTouches[0], holdTimeout = window.setTimeout("doRightClick();", 800))
}

function doRightClick() {
    rightClickPending = !1;
    var e = rightClickEvent,
        t = document.createEvent("MouseEvent");
    t.initMouseEvent("mouseup", !0, !0, window, 1, e.screenX, e.screenY, e.clientX, e.clientY, !1, !1, !1, !1, 0, null), e.target.dispatchEvent(t), (t = document.createEvent("MouseEvent")).initMouseEvent("mousedown", !0, !0, window, 1, e.screenX, e.screenY, e.clientX, e.clientY, !1, !1, !1, !1, 2, null), e.target.dispatchEvent(t), (t = document.createEvent("MouseEvent")).initMouseEvent("contextmenu", !0, !0, window, 1, e.screenX + 50, e.screenY + 5, e.clientX + 50, e.clientY + 5, !1, !1, !1, !1, 2, null), e.target.dispatchEvent(t), cancelMouseUp = !0, rightClickEvent = null
}

function iPadTouchStart(e) {
    var t = e.changedTouches[0],
        n = "mouseover",
        c = document.createEvent("MouseEvent");
    c.initMouseEvent(n, !0, !0, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(c), n = "mousedown", (c = document.createEvent("MouseEvent")).initMouseEvent(n, !0, !0, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(c), tapValid ? (window.clearTimeout(tapTimeout), t.target == lastTap ? (lastTap = null, tapValid = !1, n = "click", (c = document.createEvent("MouseEvent")).initMouseEvent(n, !0, !0, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(c), n = "dblclick", (c = document.createEvent("MouseEvent")).initMouseEvent(n, !0, !0, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(c)) : (lastTap = t.target, tapValid = !0, tapTimeout = window.setTimeout("cancelTap();", 600), startHold(e))) : (lastTap = t.target, tapValid = !0, tapTimeout = window.setTimeout("cancelTap();", 600), startHold(e))
}

function iPadTouchHandler(e) {
    var t = "";
    if (!(e.touches.length > 1)) {
        switch (e.type) {
            case "touchstart":
                if ($(e.changedTouches[0].target).is("select")) return;
                return iPadTouchStart(e), e.preventDefault(), !1;
            case "touchmove":
                cancelHold(), t = "mousemove", e.preventDefault();
                break;
            case "touchend":
                if (cancelMouseUp) return cancelMouseUp = !1, e.preventDefault(), !1;
                cancelHold(), t = "mouseup";
                break;
            default:
                return
        }
        var n = e.changedTouches[0],
            c = document.createEvent("MouseEvent");
        c.initMouseEvent(t, !0, !0, window, 1, n.screenX, n.screenY, n.clientX, n.clientY, !1, !1, !1, !1, 0, null), n.target.dispatchEvent(c), "mouseup" == t && tapValid && n.target == lastTap && ((c = document.createEvent("MouseEvent")).initMouseEvent("click", !0, !0, window, 1, n.screenX, n.screenY, n.clientX, n.clientY, !1, !1, !1, !1, 0, null), n.target.dispatchEvent(c))
    }
}
