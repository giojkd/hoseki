<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Platforms\Keywords\SQLAnywhereKeywords;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('user_id')->index()->nullable();

            $table->float('grand_total')->nullable();

            $table->string('deliverytype')->index()->nullable();#pickup o delivery
            $table->integer('is_paid')->index()->nullable();#

            $table->integer('orderstatus_id')->default(1)->index()->nullable();

            $table->timestamp('delivery_time')->nullable();
            $table->json('delivery_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}



#Pagamento alla consegna DELIVERY SI O NO
#Pagamento alla consegna TAKE WAY SI O NO
#Note per pick up
#Note per delivery
