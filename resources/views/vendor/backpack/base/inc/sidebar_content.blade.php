<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'> Prodotti</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'> Categorie</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('offer') }}'> Offerte</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('closingdate') }}'> Date chiuse</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('opening') }}'> Orari di apertura</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'> Impostazioni</a></li>
<!--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i class='nav-icon fa fa-question'></i> Orders</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('orderrow') }}'><i class='nav-icon fa fa-question'></i> Orderrows</a></li>-->
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'> Clienti</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('orderstatus') }}'> Stati degli ordini</a></li>
<li class='nav-item'><a class='nav-link' href='{{ route('showOrders') }}'> Ordini</a></li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('reservation') }}'> Prenotazioni</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('notification') }}'> Notifiche</a></li>
