<div>
    Nuova richiesta di prenotazione
    <br>
    Data e ora: {{ $reservation->reservation_time->format('d/m/Y H:i') }}
    <br>
    Persone: {{ $reservation->participants }}
    <br>
    Cliente: {{ $reservation->printUser() }}
</div>
