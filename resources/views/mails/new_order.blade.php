

<div class="draggableOrder" data-order_id="{{ $order->id }}">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6"><h5 class="card-title">{{ ucwords($order->deliverytype)}}</h5></div>
                                        <div class="col-6 text-right">
                                            <a href="javascript:" onclick="rejectOrder({{ $order->id }})" class="showOnOrderHover text-danger">
                                                <i class="las la-ban"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <p class="card-text">
                                        <i class="las lza-clock"></i> @if($order->delivery_time) {{ $order->delivery_time->format('d/m/Y H:i') }} @else <b>ASAP</b> @endif
                                        <br>
                                    @if($order->deliverytype == 'delivery')

                                            <i class="las la-map-marker"></i> {{ $order->delivery_address['value'] }}
                                        <br>
                                    @endif

                                        <i class="las la-user"></i> {{ $order->user->name }}
                                        <i class="las la-phone"></i> {{ $order->user->phone }}
                                    <br>
                                        <i class="las la-wallet"></i> @if($order->is_paid) Pagato @else NON Pagato @endif
                                        <br>
                                        <i class="las la-user"></i> {{$order->user->name }} {{$order->user->surname }}
                                        <a href="javascript:" onclick="$(this).closest('.card').attr('id','section-to-print'); window.print();"><i class="las la-print"></i></a>
                                    </p>
                                </div>
                                <ul class="list-group list-group-flush">
                                    @foreach($order->rows as $row)
                                        <li class="list-group-item">{{ $row->product->name }} <b>x{{ $row->quantity }}</b></li>
                                    @endforeach
                                </ul>
                                @if(!is_null($order->notes))
                                <div class="card-footer text-muted">
                                Note: {{ $order->notes }}
                                </div>
                                @endif

                                @if($showButtons)
                                    <a style="background:green; padding: 10px 15px; border-radius: 6px; color:#fff; font-weight:bold" href="{{url('/admin/order-status-set/'.$order->id.'/2')}}">Accetta</a>
                                    <a style="background:red; padding: 10px 15px; border-radius: 6px; color:#fff; font-weight:bold" href="{{url('/admin/order-status-set/'.$order->id.'/6')}}">Rifiuta</a>
                                @endif
                                <!-- <div class="card-body">
                                   <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>-->

                            </div>
                        </div>
