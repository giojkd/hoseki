@extends('admin')


@section('header')
  <div class="container-fluid">
    <h2>
      <span class="text-capitalize">Ordini</span>
      <small id="datatable_info_stack">Gestisci gli ordini</small>
    </h2>
  </div>
  <div class="container-fluid">
      <div class="row">
        @foreach($statuses as $status)
            <div class="col">
                <h2>{{ $status->name }}</h2>
                <div class="draggableList" data-status_id="{{ $status->id }}">
                    @if(isset($orders[$status->id]))
                        @foreach($orders[$status->id] as $order)
                        <div class="draggableOrder" data-order_id="{{ $order->id }}">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6"><h5 class="card-title">{{ ucwords($order->deliverytype)}}</h5></div>
                                        <div class="col-6 text-right">
                                            <a href="javascript:" onclick="rejectOrder({{ $order->id }})" class="showOnOrderHover text-danger">
                                                <i class="las la-ban"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <p class="card-text">
                                        <i class="las lza-clock"></i> @if($order->delivery_time) {{ $order->delivery_time->format('d/m/Y H:i') }} @else <b>ASAP</b> @endif
                                        <br>
                                    @if($order->deliverytype == 'delivery')

                                            <i class="las la-map-marker"></i> {{ $order->delivery_address['value'] }}
                                        <br>
                                    @endif
                                    @if(isset($order->user))
                                        <i class="las la-phone"></i> {{ $order->user->phone }}
                                        @endif
                                    <br>
                                        <i class="las la-wallet"></i> @if($order->is_paid) Pagato @else NON Pagato @endif
                                        <br>
                                        @if(isset($order->user))
                                        <i class="las la-user"></i> @if(!is_null($order->user)) {{$order->user->name }} {{$order->user->surname }} @endif
                                        @endif
                                        <a target="_blank" href="{{ Route('printOrder',['orderId' => $order->id]) }}"><i class="las la-print"></i></a>
                                    </p>
                                </div>
                                <ul class="list-group list-group-flush">
                                    @foreach($order->rows as $row)
                                        @if(!is_null($row->product))
                                            <li class="list-group-item">{{ $row->product->name }} <b>x{{ $row->quantity }}</b></li>
                                        @endif
                                    @endforeach
                                </ul>
                                @if(!is_null($order->notes))
                                <div class="card-footer text-muted">
                                Note: {{ $order->notes }}
                                </div>
                                @endif
                                <!-- <div class="card-body">
                                   <a href="#" class="card-link">Card link</a>
                                    <a href="#" class="card-link">Another link</a>
                                </div>-->

                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endforeach
      </div>
  </div>

  <style>
      @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
      </style>
@endsection




@section('after_styles')
  <!-- DATA TABLES -->
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/form.css') }}">
  <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/list.css') }}">

  <!-- CRUD LIST CONTENT - crud_list_styles stack -->
  @stack('crud_list_styles')
@endsection

@section('after_scripts')


  <script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/form.js') }}"></script>
  <script src="{{ asset('packages/backpack/crud/js/list.js') }}"></script>

  <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
  @stack('crud_list_scripts')

  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

  <style>
    .draggableList{
        min-height: 70px;
        border: 1px solid #ddd;
        border-radius: 3px;
        padding: 4px
    }
    .draggableList .draggableOrder .card{
        margin-bottom: 3px;
    }
    .draggableList .draggableOrder:last-of-type .card{
        margin-bottom: 0px;
    }
    .draggableOrder .showOnOrderHover{
        display: none;
    }
    .draggableOrder:hover .showOnOrderHover{
        display: inline-block;
    }
  </style>

  <script src="{{ asset('js/gomc-touch.js') }}"></script>

  <script>


    function rejectOrder(id){
        if(confirm('Sei sicuro di voler rifiutare questo ordine?')){
            $.get('{{ route('updateOrderStatus') }}/'+id+'/6',{},function(r){
                location.reload();
            })
        }
    }

      $( function() {

            $( ".draggableList" ).sortable({
            connectWith: ".draggableList",
            receive:function(e,ui){
                console.log('status '+e.target.dataset.status_id)
                console.log('order '+ui.item[0].dataset.order_id);
                $.get('/admin/order-status-set/'+ui.item[0].dataset.order_id+'/'+e.target.dataset.status_id)
            },
            }).disableSelection();
        } );
  </script>

@endsection
