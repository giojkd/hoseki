<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'La tua password è stata resettata!',
    'sent' => 'Ti abbiamo inviato un collegamento per resettare la password!',
    'throttled' => 'Per favore aspetta prima di riprovare.',
    'token' => 'Il token di reset della password non è valido.',
    'user' => "Non possiamo trovare nessun utente con quell'indirizzo email.",

];
